@extends('layouts.app')
@section('contenu')
    <form method="POST">
        {{ csrf_field() }}
        <input type="text" name="nom" placeholder="Nom">
        <input type="text" name="prenom" placeholder="Prénom">
        <input type="email" name="email" placeholder="Email">
        <input type="password" name="password" placeholder="Mot de passe">
        <input type="password" name="conf_pass" placeholder="Confirmation mot de passe">
        <button type="submit">Valider</button>
    </form>

@endsection
